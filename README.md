# ToDoList

This program is a simple JavaScript to-do list application. Its functionality supports creating, reading, updating, and deleting to-do list items.