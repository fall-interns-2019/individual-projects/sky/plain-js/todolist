/*
        This program is a simple JavaScript to-do list application. Its functionality supports creating,
        reading, updating, and deleting to-do list items.
 */
let items = [];
function addItem(input) {
    /*
            Adds a to-do item to items, the global to-do list. Item objects are set with an unique id of the
            current time and a completion value of false.

            Args:
                    input: string value of user input (previously trimmed during event action)
            Returns:
                    N/A (void function)
     */
    const item = {input, completion: false, id: Date.now(),};
    items.push(item);
    const list = document.querySelector('.js-todo-list');
    const className = "js-word" + item.id;
    list.insertAdjacentHTML('beforeend', `
    <li class="todo-item" data-key="${item.id}">
      <input id="${item.id}" type="checkbox"/>
      <label for="${item.id}" class="tick js-tick"></label>
      <span class="${className}">${item.input}</span>
      <button class="delete-todo js-delete-todo">
        <svg><use href="#delete-icon"></use></svg>
      </button>
    </li>
  `);
    $(list).find('.' + className).click(spanClick);
}
function spanClick() {
    /*
            Functionality for when a to-do item is clicked and blurred. Item is replaced with an input tag for
            editing the item's input value. After editing, the item is replaced back to a span tag.

            Args:
                    N/A
            Returns:
                    N/A (void function)
     */
    const itemKey = event.target.parentElement.dataset.key;
    const index = items.findIndex(item => item.id === Number(itemKey));
    const className2 = "js-text" + items[index].id;
    $(this).parent().replaceWith(`<li class="todo-item">
      <input id="${items[index].id}" type="checkbox"/>
      <label for="${items[index].id}" class="tick js-tick"></label>
      <input class="${className2}" id="userInput" type="text" autofocus="true" placeholder="${items[index].input}"/>
      <button class="delete-todo js-delete-todo">
        <svg><use href="#delete-icon"></use></svg>
      </button>
    </li>`);
    const list = document.querySelector('.js-todo-list');
    $(list).find('.' + className2).blur(function (item){
        let edit = document.getElementById("userInput").value;
        items[index].input = edit;
        $(this).parent().replaceWith(`<li class="todo-item" data-key="${items[index].id}">
      <input id="${items[index].id}" type="checkbox"/>
      <label for="${items[index].id}" class="tick js-tick"></label>
      <span id="newSpan" class="js-word">${items[index].input}</span>
      <button class="delete-todo js-delete-todo">
        <svg><use href="#delete-icon"></use></svg>
      </button>
    </li>`);
    });
}
function deleteItem(key) {
    /*
            Deletes to-do item from items, the global to-do list. The item's key is used to find its index
            to remove it from the to-do list. If the to-do list is empty, the HTML of the list is cleared.

            Args:
                    N/A
            Returns:
                    N/A (void function)
     */
    items.splice(items.findIndex(item => item.id === Number(key)), 1);
    const item = document.querySelector(`[data-key='${key}']`);
    item.remove();
    const list = document.querySelector('.js-todo-list');
    if (items.length === 0) { list.innerHTML = ''; }
}
function toggleDone(key) {
    /*
            Functionality for when a to-do item is checked for completion. The item's completion attribute and
            classList is properly updated.

            Args:
                    N/A
            Returns:
                    N/A (void function)
     */
    const index = items.findIndex(item => item.id === Number(key));
    items[index].completion = !items[index].completion;
    const item = document.querySelector(`[data-key='${key}']`);
    items[index].completion ? item.classList.add('done') : item.classList.remove('done');
}
const form = document.querySelector(".js-form");
if (form) {
    /*
            Functionality for when a to-do item is submitted to the form. The input is trimmed, passed to
            the addItem function, and the buffer is cleared for more input.
     */
    form.addEventListener('submit', event => {
        event.preventDefault();
        const input = document.querySelector(".js-todo-input");
        const text = input.value.trim();
        if (text !== '') {
            addItem(text);
            input.value = '';
            input.focus();
        }
    });
}
const list = document.querySelector(".js-todo-list");
if (list) {
    /*
            Functionality for when items, the global to-do list, is clicked. The event is determined, either
            being a click on the completion or deletion box, and the appropriate action is taken depending
            on which event occurred.
     */
    list.addEventListener('click', event => {
        if (event.target.classList.contains('js-tick')) {
            const itemKey = event.target.parentElement.dataset.key;
            toggleDone(itemKey);
        }
        if (event.target.classList.contains('js-delete-todo')) {
            const itemKey = event.target.parentElement.dataset.key;
            deleteItem(itemKey);
        }
    });
}